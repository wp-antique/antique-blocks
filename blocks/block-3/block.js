(function (blocks, i18n, element, components, blockEditor, _) {
    var __ = i18n.__;
    var el = element.createElement;
    var RichText = blockEditor.RichText;
    var MediaUpload = blockEditor.MediaUpload;
    var useBlockProps = blockEditor.useBlockProps;
    var BlockControls = blockEditor.BlockControls;
    var URLInput = blockEditor.URLInput;

    blocks.registerBlockType('antique-link-boxes/block-3', {
        title: __('Antique Link Box 3', 'antique-link-boxes'),
        icon: 'cover-image',
        category: 'widgets',
        attributes: {
            mediaID: {
                type: 'number'
            },
            mediaURL: {
                type: 'string',
                source: 'attribute',
                selector: 'img',
                attribute: 'src'
            },
            title: {
                type: 'array',
                source: 'children',
                selector: '.antique-link-box-3-title'
            },
            link: {
                type: 'string',
                source: 'attribute',
                selector: 'a',
                attribute: 'href'
            },
            ref: {
                type: 'array',
                source: 'children',
                selector: '.antique-link-boxes-ref'
            }
        },

        edit: function (props) {
            var attributes = props.attributes;

            function onChangeTitle(newTitle) {
                props.setAttributes({title: newTitle});
            }

            function onChangeLink(newLink, post) {
                props.setAttributes({link: newLink});
            }

            function onChangeRef(newRef) {
                props.setAttributes({ref: newRef});
            }

            function onSelectImage(media) {
                return props.setAttributes({
                    mediaURL: media.url,
                    mediaID: media.id
                });
            }

            function RemoveImage() {
                return props.setAttributes({
                    mediaURL: null,
                    mediaID: null
                });
            }

            return [
                el('div', useBlockProps(),
                        el(BlockControls, {},
                                el(URLInput, {
                                    value: attributes.link, onChange: onChangeLink
                                })),
                        // Here, antique-link-boxes doesn't have its own div,
                        // but that makes no difference since no shadow is
                        // displayed on the back-end.

                        el('div', {
                            className: 'antique-link-box-3 antique-link-boxes'
                        },
                                el('div', {
                                    className: 'antique-link-box-3-heading-wrap antique-link-boxes-heading-wrap'
                                },
                                        el('div', {
                                            className: 'antique-link-box-3-title-wrap'
                                        },
                                                el(RichText, {
                                                    tagName: 'span',
                                                    className: 'antique-link-box-3-title',
                                                    value: attributes.title,
                                                    placeholder: __('Title', 'antique-link-boxes'),
                                                    onChange: onChangeTitle
                                                })
                                                ),
                                        ),
                                el(MediaUpload, {
                                    onSelect: onSelectImage,
                                    allowedTypes: 'image',
                                    value: attributes.mediaID,
                                    render: function (obj) {
                                        return el('div', {
                                            className: 'antique-link-box-3-img-wrap antique-link-boxes-img-wrap'
                                        },
                                                attributes.mediaID &&
                                                el('img', {
                                                    src: attributes.mediaURL
                                                }),
                                                el('div', {
                                                    className: !attributes.mediaID
                                                            ? 'antique-link-boxes-button-wrap'
                                                            : 'antique-link-boxes-button-wrap antique-link-boxes-remove-button-wrap'},
                                                        !attributes.mediaID &&
                                                        el(
                                                                components.Button,
                                                                {
                                                                    className: 'button button-large upload-button',
                                                                    onClick: obj.open
                                                                },
                                                                __('Choose image', 'antique-link-boxes')
                                                                ),
                                                        attributes.mediaID &&
                                                        el(
                                                                components.Button,
                                                                {
                                                                    className: 'button button-large remove-button',
                                                                    onClick: RemoveImage
                                                                },
                                                                __('Remove image', 'antique-link-boxes')
                                                                )
                                                        )
                                                );
                                    }
                                })
                                ),
                        el('div', {
                            className: 'antique-link-boxes-ref-wrap'
                        },
                                el('span', {}, __('Reference:', 'antique-link-boxes')),
                                el(RichText, {
                                    tagName: 'p',
                                    className: 'antique-link-boxes-ref',
                                    value: attributes.ref,
                                    placeholder: __('...', 'antique-link-boxes'),
                                    onChange: onChangeRef
                                }
                                )

                                )

                        )];
        },

        save: function (props) {
            var attributes = props.attributes;

            // Is useBlockProps.save({...}) necessary?
            return [
                el('div', {className: 'antique-link-boxes'},
                        el('div', {
                            className: 'antique-link-box-3'
                        },
                                el('div', {
                                    className: 'antique-link-box-3-heading-wrap antique-link-boxes-heading-wrap'
                                },
                                        el('div', {
                                            className: 'antique-link-box-3-title-wrap'},
                                                el(RichText.Content, {
                                                    tagName: 'span',
                                                    className: 'antique-link-box-3-title',
                                                    value: attributes.title
                                                })
                                                ),
                                        ),
                                el('div', {
                                    className: 'antique-link-box-3-img-wrap antique-link-boxes-img-wrap'
                                },
                                        el('div', {
                                            className: 'antique-link-boxes-ref-indicator'
                                        }),
                                        attributes.mediaURL &&
                                        el('img', {src: attributes.mediaURL})
                                        ),
                                el('a', attributes.link ? {
                                    href: attributes.link,
                                    className: 'antique-link-boxes-link'
                                }
                                : {
                                    className: 'antique-link-boxes-no-link'
                                })
                                )
                        ),
                el(RichText.Content, {
                    tagName: 'p',
                    className: 'antique-link-boxes-ref',
                    value: attributes.ref
                })
            ];
        }
    });
})(
        window.wp.blocks,
        window.wp.i18n,
        window.wp.element,
        window.wp.components,
        window.wp.blockEditor,
        window._
        );