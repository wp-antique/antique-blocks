(function () {
    // Reference options
    var ref_options = antique_link_boxes_ref_options;

    if (ref_options.enabled !== 'on') {
        return;
    }

    // Get references of all blocks
    var block_ref = document.querySelectorAll('.antique-link-boxes-ref');
    if (!block_ref.length) {
        return;
    }

    // Get reference indicator elements of all blocks
    var block_ref_indicator = document.querySelectorAll('.antique-link-boxes-ref-indicator');
    if (!block_ref_indicator.length) {
        return;
    }

    // References are placed below block
    if (ref_options.position === 'ref_block') {

        // Iterate through all blocks
        for (var i = 0; i < block_ref.length; i++) {

            // Retrieve reference text
            ref_text = block_ref[i].innerHTML;

            if (ref_text !== '') {
                // Display reference if not empty
                block_ref[i].style.display = 'block';
            } else {
                // Else remove empty reference from block
                block_ref[i].remove();
            }
            // Remove reference indicator element
            block_ref_indicator[i].remove();
        }

        // References and images are enumerated, list is placed at end of page
    } else if (ref_options.position === 'ref_page') {

        // Brackets or font style enclosing the reference number
        var ref_open_num;
        var ref_close_num;

        // Determine numbering
        if (ref_options.numbering === '[n]') {
            ref_open_num = '[';
            ref_close_num = '] ';
        } else if (ref_options.numbering === '(n)') {
            ref_open_num = '(';
            ref_close_num = ') ';
        } else {
            ref_open_num = '<b>';
            ref_close_num = '</b> ';
        }

        // Create paragraph as list of references
        var ref_list = document.createElement('p');
        ref_list.classList.add('antique-link-boxes-ref-list');

        // Iterate through all blocks
        k = 1;
        for (var i = 0; i < block_ref.length; i++) {

            // Set number and retrieve reference text
            ref_number = ref_open_num + k.toString() + ref_close_num;
            ref_text = block_ref[i].innerHTML;

            if (ref_text !== '') {
                // If reference text is not empty, add it to list and enumerate image and reference
                ref_list.innerHTML += ref_number + ref_text + '<br>';
                block_ref_indicator[i].innerHTML = k.toString();
                block_ref_indicator[i].style.display = 'block';

                k += 1;
            } else {
                // Else remove reference indicator element
                block_ref_indicator[i].remove();
            }

            // Remove reference from block
            block_ref[i].remove();
        }

        // Set list of references at end of page
        var content = document.getElementById('page-content');
        content.appendChild(ref_list);
    }
})($);