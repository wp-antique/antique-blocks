(function () {
    const is_shadow_on = antique_link_boxes_shadow_options.is_shadow_on;

    if (is_shadow_on) {
        var antique_link_boxes = document.getElementsByClassName('antique-link-boxes');
        for (var i = 0; i < antique_link_boxes.length; i++) {
            antique_link_boxes[i].classList.add('antique-link-boxes-shadow');
        }
    }
})($);