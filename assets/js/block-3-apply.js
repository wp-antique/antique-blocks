(function () {
    const options = antique_link_box_3_options;

    const title_tag = options.title_tag;
    const block_has_border = options.block_has_border;
    const block_has_folded_corner = options.block_has_folded_corner;
    const img_has_padding = options.img_has_padding;
    const ref_has_border = options.ref_has_border;

    const allowed_title_tags = ['p', 'h2', 'h3'];

    var antique_link_boxes = document.getElementsByClassName('antique-link-box-3');

    if (!antique_link_boxes) {
        return;
    }

    if (allowed_title_tags.includes(title_tag)) {
        var opening_tag = '<' + title_tag + ' class="antique-link-box-3-title">';
        var closing_tag = '</' + title_tag + '>';

        for (var i = 0; i < antique_link_boxes.length; i++) {
            var title = antique_link_boxes[i].querySelector('.antique-link-box-3-title');
            if (title) {
                title.outerHTML = opening_tag + title.innerHTML + closing_tag;
            }
        }
    }

    if (block_has_border) {
        for (var i = 0; i < antique_link_boxes.length; i++) {
            antique_link_boxes[i].classList.add('has-border');
        }
    }

    if (block_has_folded_corner) {
        for (var i = 0; i < antique_link_boxes.length; i++) {
            antique_link_boxes[i].classList.add('has-folded-corner');
        }
    }

    if (img_has_padding) {
        for (var i = 0; i < antique_link_boxes.length; i++) {
            var img = antique_link_boxes[i].querySelector('img');
            if (img) {
                img.classList.add('has-padding');
            }
        }
    }

    if (ref_has_border) {
        for (var i = 0; i < antique_link_boxes.length; i++) {
            var ref_indicator = antique_link_boxes[i].querySelector('.antique-link-boxes-ref-indicator');
            if (ref_indicator) {
                ref_indicator.classList.add('has-border');
            }
        }
    }
})($);