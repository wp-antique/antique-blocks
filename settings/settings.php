<?php
include plugin_dir_path(__FILE__) . '/fields.php';

/*
  ---------------------------------
  Styles and Scripts in Admin Area
  ---------------------------------
 */

add_action(
        hook_name: 'admin_enqueue_scripts',
        callback: 'antique_link_boxes_settings_style'
);

function antique_link_boxes_settings_style() {

    $handle = 'antique-link-boxes-settings-style';

    wp_register_style(
            handle: $handle,
            src: plugins_url('/css/settings.css', __FILE__),
            deps: array(),
            ver: false,
            media: 'all'
    );

    wp_enqueue_style(handle: $handle);
}

add_action(
        hook_name: 'admin_enqueue_scripts',
        callback: 'antique_link_boxes_settings_script'
);

function antique_link_boxes_settings_script($hook) {

    $handle = 'antique-link-boxes-settings-script';

    wp_register_script(
            handle: $handle,
            src: plugins_url('/js/settings.js', __FILE__),
            deps: array(),
            ver: false,
            in_footer: true
    );

    wp_enqueue_script(handle: $handle);
}

/*
  ---------------------------------
  Settings Page
  ---------------------------------
 */

add_action(
        hook_name: 'admin_menu',
        callback: 'antique_link_boxes_options_page'
);

function antique_link_boxes_options_page() {

    if (!function_exists('antique_plugins_admin_menu')) {
        add_menu_page(
                page_title: __('Antique Link Boxes', 'antique-link-boxes'),
                menu_title: 'Antique<br> ' . __('Link Boxes', 'antique-link-boxes'),
                capability: 'manage_options',
                menu_slug: 'antique_link_boxes',
                callback: 'antique_link_boxes_options_page_html',
        );
    } else {
        add_submenu_page(
                parent_slug: 'antique_plugins',
                page_title: __('Antique Plugins', 'antique-link-boxes')
                . ' &rsaquo; ' . __('Link Boxes', 'antique-link-boxes'),
                menu_title: __('Link Boxes', 'antique-link-boxes'),
                capability: 'manage_options',
                menu_slug: 'antique_link_boxes',
                callback: 'antique_link_boxes_options_page_html'
        );
    }
}

add_action(
        hook_name: 'admin_init',
        callback: 'antique_link_boxes_remove_menu_page'
);

function antique_link_boxes_remove_menu_page() {
    if (!function_exists('antique_plugins_admin_menu')) {
        remove_menu_page(menu_slug: 'antique_link_boxes');
    }
}

function antique_link_boxes_options_page_html() {

    if (!current_user_can(capability: 'manage_options')) {
        return;
    }
    ?>
    <div class="wrap antique-link-boxes-options-wrap">
        <h1><?php echo esc_html(get_admin_page_title()); ?></h1>

        <?php settings_errors(setting: 'antique_link_boxes_updated_message'); ?>
        <?php settings_errors(setting: 'antique_link_boxes_reset_message'); ?>

        <?php $active_tab = isset($_GET['tab']) ? $_GET['tab'] : 'general_options'; ?>

        <h2 class="nav-tab-wrapper">
            <a href="?page=antique_link_boxes&tab=general_options"
               class="nav-tab <?php echo $active_tab == 'general_options' ? 'nav-tab-active' : ''; ?>"
               >General</a>
            <a href="?page=antique_link_boxes&tab=block_1"
               class="nav-tab <?php echo $active_tab == 'block_1' ? 'nav-tab-active' : ''; ?>"
               >Block 1</a>
            <a href="?page=antique_link_boxes&tab=block_2"
               class="nav-tab <?php echo $active_tab == 'block_2' ? 'nav-tab-active' : ''; ?>"
               >Block 2</a>
            <a href="?page=antique_link_boxes&tab=block_3"
               class="nav-tab <?php echo $active_tab == 'block_3' ? 'nav-tab-active' : ''; ?>"
               >Block 3</a>
        </h2>

        <form method="post" action="options.php">

            <?php
            if ($active_tab == 'general_options') {
                settings_fields(option_group: 'general_options');
                do_settings_sections(page: 'general_settings');
                ?>
                <div class="antique-link-boxes-submit-buttons-wrap">
                    <?php
                    submit_button(
                            text: __('Save changes', 'antique-link-boxes'),
                            type: 'primary',
                            name: 'updated',
                            wrap: false
                    );
                    submit_button(
                            text: __('Reset', 'antique-link-boxes'),
                            type: 'secondary',
                            name: 'reset_general',
                            wrap: false
                    );
                    ?>
                </div>
                <?php
                antique_link_boxes_where_is_used();
            } else if ($active_tab == 'block_1') {
                settings_fields(option_group: 'block_1');
                do_settings_sections(page: 'block_1_settings');
                ?>
                <div class="antique-link-boxes-submit-buttons-wrap">
                    <?php
                    submit_button(
                            text: __('Save changes', 'antique-link-boxes'),
                            type: 'primary',
                            name: 'updated',
                            wrap: false
                    );
                    submit_button(
                            text: __('Reset', 'antique-link-boxes'),
                            type: 'secondary',
                            name: 'reset_block_1',
                            wrap: false
                    );
                    ?>
                </div>
                <?php
            } else if ($active_tab == 'block_2') {
                settings_fields(option_group: 'block_2');
                do_settings_sections(page: 'block_2_settings');
                ?>
                <div class="antique-link-boxes-submit-buttons-wrap">
                    <?php
                    submit_button(
                            text: __('Save changes', 'antique-link-boxes'),
                            type: 'primary',
                            name: 'updated',
                            wrap: false
                    );
                    submit_button(
                            text: __('Reset', 'antique-link-boxes'),
                            type: 'secondary',
                            name: 'reset_block_2',
                            wrap: false
                    );
                    ?>
                </div>
                <?php
            } else if ($active_tab == 'block_3') {
                settings_fields(option_group: 'block_3');
                do_settings_sections(page: 'block_3_settings');
                ?>
                <div class="antique-link-boxes-submit-buttons-wrap">
                    <?php
                    submit_button(
                            text: __('Save changes', 'antique-link-boxes'),
                            type: 'primary',
                            name: 'updated',
                            wrap: false
                    );
                    submit_button(
                            text: __('Reset', 'antique-link-boxes'),
                            type: 'secondary',
                            name: 'reset_block_3',
                            wrap: false
                    );
                    ?>
                </div>
                <?php
            }
            ?>
        </form>
    </div>

    <?php
}

function antique_link_boxes_settings_sanitize_cb($input) {

    if (isset($_POST['updated'])) {

        add_settings_error(
                setting: 'antique_link_boxes_updated_message',
                code: 'antique_link_boxes_updated_message_code',
                message: __('Your settings have been saved.', 'antique-link-boxes'),
                type: 'updated'
        );
    } else if (isset($_POST['reset_general'])) {

        add_settings_error(
                setting: 'antique_link_boxes_reset_message',
                code: 'antique_link_boxes_reset_message_code',
                message: __('Your settings have been reset.', 'antique-link-boxes'),
                type: 'updated'
        );

        $defaults = get_antique_link_boxes_default_general_options();
        return $defaults;
    } else if (isset($_POST['reset_block_1'])) {

        add_settings_error(
                setting: 'antique_link_boxes_reset_message',
                code: 'antique_link_boxes_reset_message_code',
                message: __('Your settings have been reset.', 'antique-link-boxes'),
                type: 'updated'
        );

        $defaults = get_antique_link_boxes_default_single_options(
                option_name: 'antique_link_box_1_options'
        );
        return $defaults;
    } else if (isset($_POST['reset_block_2'])) {

        add_settings_error(
                setting: 'antique_link_boxes_reset_message',
                code: 'antique_link_boxes_reset_message_code',
                message: __('Your settings have been reset.', 'antique-link-boxes'),
                type: 'updated'
        );

        $defaults = get_antique_link_boxes_default_single_options(
                option_name: 'antique_link_box_2_options'
        );
        return $defaults;
    } else if (isset($_POST['reset_block_3'])) {

        add_settings_error(
                setting: 'antique_link_boxes_reset_message',
                code: 'antique_link_boxes_reset_message_code',
                message: __('Your settings have been reset.', 'antique-link-boxes'),
                type: 'updated'
        );

        $defaults = get_antique_link_boxes_default_single_options(
                option_name: 'antique_link_box_3_options'
        );
        return $defaults;
    }

    return $input;
}

function antique_link_boxes_where_is_used() {

    $search_for = array(
        '<!-- wp:antique-link-boxes/block-1 ',
        '<!-- wp:antique-link-boxes/block-2 ',
        '<!-- wp:antique-link-boxes/block-3 ',
    );

    $query = new WP_Query(array(
        'post_type' => array('post', 'page'),
        'posts_per_page' => -1
    ));

    $pages = array();

    if ($query->have_posts()) {
        while ($query->have_posts()) {

            $query->the_post();
            $id = get_the_ID();

            foreach ($search_for as $s) {
                $is_in_content = strpos(
                        haystack: get_the_content($id),
                        needle: $s
                );

                if ($is_in_content) {
                    $post_type_en = get_post_type();
                    if ($post_type_en == 'page') {
                        $post_type = esc_html__('page', 'antique-link-boxes');
                    } else if ($post_type_en == 'post') {
                        $post_type = esc_html__('post', 'antique-link-boxes');
                    } else {
                        $post_type = $post_type_en;
                    }

                    $pages[$id] = array(
                        'post_type' => $post_type,
                        'title' => get_the_title(),
                        'loc' => esc_html__('content', 'antique-link-boxes'),
                        'link' => get_permalink(),
                    );

                    continue;
                }
            }
        }
    }

    ksort($pages);

    wp_reset_postdata();
    ?>

    <div class="antique-plugin-table-wrap">
        <p><?php
            esc_html_e('Pages and posts containing one of the blocks:',
                    'antique-link-boxes');
            ?></p>
        <div id="antique-plugin-toggle-table">
            <span class="show is-displayed"
                  ><?php esc_html_e('Show', 'antique-link-boxes'); ?></span>
            <span class="hide"
                  ><?php esc_html_e('Hide', 'antique-link-boxes'); ?></span>
        </div>

        <table class="antique-plugin-pages-table">
            <tr>
                <th><?php esc_html_e('ID', 'antique-link-boxes'); ?></th>
                <th><?php esc_html_e('type', 'antique-link-boxes'); ?></th>
                <th><?php esc_html_e('title', 'antique-link-boxes'); ?></th>
                <th><?php esc_html_e('location', 'antique-link-boxes'); ?></th>
                <th><?php esc_html_e('link', 'antique-link-boxes'); ?></th>
            </tr>
            <?php
            foreach ($pages as $ID => $page) {
                ?>
                <tr>
                    <td><?php esc_html_e($ID); ?></td>
                    <td><?php echo $page['post_type']; ?></td>
                    <td><?php echo $page['title']; ?></td>
                    <td><?php echo $page['loc']; ?></td>
                    <td><a href="<?php echo $page['link']; ?>"
                           target="_blank"><?php
                               esc_html_e('link', 'antique-link-boxes');
                               ?></a></td>
                </tr>
                <?php
            }
            ?>
        </table>
    </div>

    <?php
}

/*
  ---------------------------------
  General Settings
  ---------------------------------
 */

function antique_link_boxes_general_options_subpage() {

    add_submenu_page(
            parent_slug: 'antique_link_boxes',
            page_title: 'General',
            menu_title: 'General',
            capability: 'manage_options',
            menu_slug: 'general_settings',
            callback: ''
    );
}

add_action(
        hook_name: 'admin_init',
        callback: 'antique_link_boxes_general_options_subpage_init'
);

function antique_link_boxes_general_options_subpage_init() {

    $option_name = 'antique_link_boxes_general_options';

    register_setting(
            option_group: 'general_options',
            option_name: $option_name,
            args: array(
                'sanitize_callback' => 'antique_link_boxes_settings_sanitize_cb'
            )
    );

    add_settings_section(
            id: 'antique_link_boxes_style_section',
            title: __('Style', 'antique-link-boxes'),
            callback: 'antique_link_boxes_style_section_cb',
            page: 'general_settings'
    );

    add_settings_field(
            id: 'border_radius',
            title: __('Border radius:', 'antique-link-boxes'),
            callback: 'antique_link_boxes_field_border_radius_cb',
            page: 'general_settings',
            section: 'antique_link_boxes_style_section',
            args: array(
                'option_name' => $option_name,
                'label_for' => 'border_radius',
            )
    );

    add_settings_field(
            id: 'shadow_bool',
            title: __('Shadow:', 'antique-link-boxes'),
            callback: 'antique_link_boxes_field_shadow_bool_cb',
            page: 'general_settings',
            section: 'antique_link_boxes_style_section',
            args: array(
                'option_name' => $option_name,
                'label_for' => 'shadow_bool',
            )
    );

    add_settings_section(
            id: 'antique_link_boxes_ref_section',
            title: __('References', 'antique-link-boxes'),
            callback: 'antique_link_boxes_ref_section_cb',
            page: 'general_settings'
    );

    add_settings_field(
            id: 'ref_bool',
            title: __('Enable:', 'antique-link-boxes'),
            callback: 'antique_link_boxes_field_ref_cb',
            page: 'general_settings',
            section: 'antique_link_boxes_ref_section',
            args: array(
                'option_name' => $option_name,
                'label_for' => 'ref_bool',
            )
    );

    add_settings_field(
            id: 'ref_position',
            title: __('Position:', 'antique-link-boxes'),
            callback: 'antique_link_boxes_field_ref_position_cb',
            page: 'general_settings',
            section: 'antique_link_boxes_ref_section',
            args: array(
                'option_name' => $option_name,
                'label_for' => 'ref_position',
            )
    );

    add_settings_field(
            id: 'ref_numbering',
            title: __('Numbering:', 'antique-link-boxes'),
            callback: 'antique_link_boxes_field_ref_numbering_cb',
            page: 'general_settings',
            section: 'antique_link_boxes_ref_section',
            args: array(
                'option_name' => $option_name,
                'label_for' => 'ref_numbering',
            )
    );
}

/*
  ---------------------------------
  Settings for Block 1
  ---------------------------------
 */

function antique_link_box_1_options_subpage() {

    add_submenu_page(
            parent_slug: 'antique_link_boxes',
            page_title: 'Settings for Block 1',
            menu_title: 'Block 1',
            capability: 'manage_options',
            menu_slug: 'block_1_settings',
            callback: ''
    );
}

add_action(
        hook_name: 'admin_init',
        callback: 'antique_link_box_1_options_subpage_init'
);

function antique_link_box_1_options_subpage_init() {

    $page = 'block_1_settings';
    $option_group = 'block_1';
    $option_name = 'antique_link_box_1_options';
    $prefix_section = 'antique_link_box_section_';
    $prefix_callback = 'antique_link_box_';

    register_setting(
            option_group: $option_group,
            option_name: $option_name,
            args: array(
                'sanitize_callback' => 'antique_link_boxes_settings_sanitize_cb'
            )
    );

    add_settings_section(
            id: $prefix_section . 'structure',
            title: __('Structure', 'antique-link-boxes'),
            callback: $prefix_callback . 'section_structure_cb',
            page: $page,
    );

    add_settings_field(
            id: 'title_tag',
            title: __('Title tag:', 'antique_link_boxes'),
            callback: $prefix_callback . 'field_title_tag_cb',
            page: $page,
            section: $prefix_section . 'structure',
            args: array(
                'option_name' => $option_name,
                'label_for' => 'title_tag',
            )
    );

    add_settings_field(
            id: 'subtitle_tag',
            title: __('Subtitle tag:', 'antique-link-boxes'),
            callback: $prefix_callback . 'field_subtitle_tag_cb',
            page: $page,
            section: $prefix_section . 'structure',
            args: array(
                'option_name' => $option_name,
                'label_for' => 'subtitle_tag',
            )
    );

    add_settings_section(
            id: $prefix_section . 'style',
            title: __('Style', 'antique-link-boxes'),
            callback: $prefix_callback . 'section_style_cb',
            page: $page
    );

    add_settings_field(
            id: 'color',
            title: __('Font color:', 'antique-link-boxes'),
            callback: $prefix_callback . 'field_color_cb',
            page: $page,
            section: $prefix_section . 'style',
            args: array(
                'option_name' => $option_name,
                'label_for' => 'color',
            )
    );

    add_settings_field(
            id: 'bg_color',
            title: __('Background color:', 'antique-link-boxes'),
            callback: $prefix_callback . 'field_bg_color_cb',
            page: $page,
            section: $prefix_section . 'style',
            args: array(
                'option_name' => $option_name,
                'label_for' => 'bg_color',
            )
    );

    add_settings_field(
            id: 'border',
            title: __('Border:', 'antique-link-boxes'),
            callback: $prefix_callback . 'field_border_cb',
            page: $page,
            section: $prefix_section . 'style',
            args: array(
                'option_name' => $option_name,
                'label_for' => 'border',
            )
    );

    add_settings_field(
            id: 'folded_corner_bool',
            title: __('Folded corner:', 'antique-link-boxes'),
            callback: $prefix_callback . 'field_folded_corner_bool_cb',
            page: $page,
            section: $prefix_section . 'style',
            args: array(
                'option_name' => $option_name,
                'label_for' => 'folded_corner_bool',
            )
    );

    add_settings_field(
            id: 'img',
            title: __('Images:', 'antique-link-boxes'),
            callback: $prefix_callback . 'field_img_cb',
            page: $page,
            section: $prefix_section . 'style',
            args: array(
                'option_name' => $option_name,
                'label_for' => 'img',
            )
    );

    $ref_checkbox = get_antique_link_boxes_option(
                    option_name: 'antique_link_boxes_general_options'
            )['ref_bool'];

    if ($ref_checkbox == 'on') {

        add_settings_section(
                id: $prefix_section . 'ref',
                title: __('References', 'antique-link-boxes'),
                callback: $prefix_callback . 'section_ref_cb',
                page: $page,
        );

        add_settings_field(
                id: 'ref_indicator_color',
                title: __('Font color:', 'antique-link-boxes'),
                callback: $prefix_callback . 'field_ref_indicator_color_cb',
                page: $page,
                section: $prefix_section . 'ref',
                args: array(
                    'option_name' => $option_name,
                    'label_for' => 'ref_indicator_color',
                )
        );

        add_settings_field(
                id: 'ref_indicator_bg_color',
                title: __('Background color:', 'antique-link-boxes'),
                callback: $prefix_callback . 'field_ref_indicator_bg_color_cb',
                page: $page,
                section: $prefix_section . 'ref',
                args: array(
                    'option_name' => $option_name,
                    'label_for' => 'ref_indicator_bg_color',
                )
        );

        add_settings_field(
                id: 'ref_indicator_border',
                title: __('Border:', 'antique-link-boxes'),
                callback: $prefix_callback . 'field_ref_indicator_border_cb',
                page: $page,
                section: $prefix_section . 'ref',
                args: array(
                    'option_name' => $option_name,
                    'label_for' => 'ref_indicator_border',
                )
        );
    }
}

/*
  ---------------------------------
  Settings for Block 2
  ---------------------------------
 */

function antique_link_box_2_options_subpage() {

    add_submenu_page(
            parent_slug: 'antique_link_boxes',
            page_title: 'Settings for Block 2',
            menu_title: 'Block 2',
            capability: 'manage_options',
            menu_slug: 'block_2_settings',
            callback: ''
    );
}

add_action(
        hook_name: 'admin_init',
        callback: 'antique_link_box_2_options_subpage_init'
);

function antique_link_box_2_options_subpage_init() {

    $page = 'block_2_settings';
    $option_group = 'block_2';
    $option_name = 'antique_link_box_2_options';
    $prefix_section = 'antique_link_box_section_';
    $prefix_callback = 'antique_link_box_';

    register_setting(
            option_group: $option_group,
            option_name: $option_name,
            args: array(
                'sanitize_callback' => 'antique_link_boxes_settings_sanitize_cb'
            )
    );

    add_settings_section(
            id: $prefix_section . 'structure',
            title: __('Structure', 'antique-link-boxes'),
            callback: $prefix_callback . 'section_structure_cb',
            page: $page,
    );

    add_settings_field(
            id: 'title_tag',
            title: __('Title tag:', 'antique-link-boxes'),
            callback: $prefix_callback . 'field_title_tag_cb',
            page: $page,
            section: $prefix_section . 'structure',
            args: array(
                'option_name' => $option_name,
                'label_for' => 'title_tag',
            )
    );

    add_settings_field(
            id: 'subtitle_tag',
            title: __('Subtitle tag:', 'antique-link-boxes'),
            callback: $prefix_callback . 'field_subtitle_tag_cb',
            page: $page,
            section: $prefix_section . 'structure',
            args: array(
                'option_name' => $option_name,
                'label_for' => 'subtitle_tag',
            )
    );

    add_settings_section(
            id: $prefix_section . 'style',
            title: __('Style', 'antique-link-boxes'),
            callback: $prefix_callback . 'section_style_cb',
            page: $page
    );

    add_settings_field(
            id: 'color',
            title: __('Font color:', 'antique-link-boxes'),
            callback: $prefix_callback . 'field_color_cb',
            page: $page,
            section: $prefix_section . 'style',
            args: array(
                'option_name' => $option_name,
                'label_for' => 'color',
            )
    );

    add_settings_field(
            id: 'bg_color',
            title: __('Background color:', 'antique-link-boxes'),
            callback: $prefix_callback . 'field_bg_color_cb',
            page: $page,
            section: $prefix_section . 'style',
            args: array(
                'option_name' => $option_name,
                'label_for' => 'bg_color',
            )
    );

    add_settings_field(
            id: 'border',
            title: __('Border:', 'antique-link-boxes'),
            callback: $prefix_callback . 'field_border_cb',
            page: $page,
            section: $prefix_section . 'style',
            args: array(
                'option_name' => $option_name,
                'label_for' => 'border',
            )
    );

    add_settings_field(
            id: 'folded_corner_bool',
            title: __('Folded corner:', 'antique-link-boxes'),
            callback: $prefix_callback . 'field_folded_corner_bool_cb',
            page: $page,
            section: $prefix_section . 'style',
            args: array(
                'option_name' => $option_name,
                'label_for' => 'folded_corner_bool',
            )
    );

    add_settings_field(
            id: 'img',
            title: __('Images:', 'antique-link-boxes'),
            callback: $prefix_callback . 'field_img_cb',
            page: $page,
            section: $prefix_section . 'style',
            args: array(
                'option_name' => $option_name,
                'label_for' => 'img',
            )
    );

    $ref_checkbox = get_antique_link_boxes_option(
                    option_name: 'antique_link_boxes_general_options'
            )['ref_bool'];

    if ($ref_checkbox == 'on') {

        add_settings_section(
                id: $prefix_section . 'ref',
                title: __('References', 'antique-link-boxes'),
                callback: $prefix_callback . 'section_ref_cb',
                page: $page,
        );

        add_settings_field(
                id: 'ref_indicator_color',
                title: __('Font color:', 'antique-link-boxes'),
                callback: $prefix_callback . 'field_ref_indicator_color_cb',
                page: $page,
                section: $prefix_section . 'ref',
                args: array(
                    'option_name' => $option_name,
                    'label_for' => 'ref_indicator_color',
                )
        );

        add_settings_field(
                id: 'ref_indicator_bg_color',
                title: __('Background color:', 'antique-link-boxes'),
                callback: $prefix_callback . 'field_ref_indicator_bg_color_cb',
                page: $page,
                section: $prefix_section . 'ref',
                args: array(
                    'option_name' => $option_name,
                    'label_for' => 'ref_indicator_bg_color',
                )
        );

        add_settings_field(
                id: 'ref_indicator_border',
                title: __('Border:', 'antique-link-boxes'),
                callback: $prefix_callback . 'field_ref_indicator_border_cb',
                page: $page,
                section: $prefix_section . 'ref',
                args: array(
                    'option_name' => $option_name,
                    'label_for' => 'ref_indicator_border',
                )
        );
    }
}

/*
  ---------------------------------
  Settings for Block 3
  ---------------------------------
 */

function antique_link_box_3_options_subpage() {

    add_submenu_page(
            parent_slug: 'antique_link_boxes',
            page_title: 'Settings for Block 3',
            menu_title: 'Block 3',
            capability: 'manage_options',
            menu_slug: 'block_3_settings',
            callback: ''
    );
}

add_action(
        hook_name: 'admin_init',
        callback: 'antique_link_box_3_options_subpage_init'
);

function antique_link_box_3_options_subpage_init() {

    $page = 'block_3_settings';
    $option_group = 'block_3';
    $option_name = 'antique_link_box_3_options';
    $prefix_section = 'antique_link_box_section_';
    $prefix_callback = 'antique_link_box_';

    register_setting(
            option_group: $option_group,
            option_name: $option_name,
            args: array(
                'sanitize_callback' => 'antique_link_boxes_settings_sanitize_cb'
            )
    );

    add_settings_section(
            id: $prefix_section . 'structure',
            title: __('Structure', 'antique-link-boxes'),
            callback: $prefix_callback . 'section_structure_cb',
            page: $page,
    );

    add_settings_field(
            id: 'title_tag',
            title: __('Title tag:', 'antique-link-boxes'),
            callback: $prefix_callback . 'field_title_tag_cb',
            page: $page,
            section: $prefix_section . 'structure',
            args: array(
                'option_name' => $option_name,
                'label_for' => 'title_tag',
            )
    );

    add_settings_section(
            id: $prefix_section . 'style',
            title: __('Style', 'antique-link-boxes'),
            callback: $prefix_callback . 'section_style_cb',
            page: $page
    );

    add_settings_field(
            id: 'color',
            title: __('Font color:', 'antique-link-boxes'),
            callback: $prefix_callback . 'field_color_cb',
            page: $page,
            section: $prefix_section . 'style',
            args: array(
                'option_name' => $option_name,
                'label_for' => 'color',
            )
    );

    add_settings_field(
            id: 'bg_color',
            title: __('Background color:', 'antique-link-boxes'),
            callback: $prefix_callback . 'field_bg_color_cb',
            page: $page,
            section: $prefix_section . 'style',
            args: array(
                'option_name' => $option_name,
                'label_for' => 'bg_color',
            )
    );

    add_settings_field(
            id: 'border',
            title: __('Border:', 'antique-link-boxes'),
            callback: $prefix_callback . 'field_border_cb',
            page: $page,
            section: $prefix_section . 'style',
            args: array(
                'option_name' => $option_name,
                'label_for' => 'border',
            )
    );

    add_settings_field(
            id: 'folded_corner_bool',
            title: __('Folded corner:', 'antique-link-boxes'),
            callback: $prefix_callback . 'field_folded_corner_bool_cb',
            page: $page,
            section: $prefix_section . 'style',
            args: array(
                'option_name' => $option_name,
                'label_for' => 'folded_corner_bool',
            )
    );

    add_settings_field(
            id: 'img',
            title: __('Images:', 'antique-link-boxes'),
            callback: $prefix_callback . 'field_img_cb',
            page: $page,
            section: $prefix_section . 'style',
            args: array(
                'option_name' => $option_name,
                'label_for' => 'img',
            )
    );

    $ref_checkbox = get_antique_link_boxes_option(
                    option_name: 'antique_link_boxes_general_options'
            )['ref_bool'];

    if ($ref_checkbox == 'on') {

        add_settings_section(
                id: $prefix_section . 'ref',
                title: __('References', 'antique-link-boxes'),
                callback: $prefix_callback . 'section_ref_cb',
                page: $page,
        );

        add_settings_field(
                id: 'ref_indicator_color',
                title: __('Font color:', 'antique-link-boxes'),
                callback: $prefix_callback . 'field_ref_indicator_color_cb',
                page: $page,
                section: $prefix_section . 'ref',
                args: array(
                    'option_name' => $option_name,
                    'label_for' => 'ref_indicator_color',
                )
        );

        add_settings_field(
                id: 'ref_indicator_bg_color',
                title: __('Background color:', 'antique-link-boxes'),
                callback: $prefix_callback . 'field_ref_indicator_bg_color_cb',
                page: $page,
                section: $prefix_section . 'ref',
                args: array(
                    'option_name' => $option_name,
                    'label_for' => 'ref_indicator_bg_color',
                )
        );

        add_settings_field(
                id: 'ref_indicator_border',
                title: __('Border:', 'antique-link-boxes'),
                callback: $prefix_callback . 'field_ref_indicator_border_cb',
                page: $page,
                section: $prefix_section . 'ref',
                args: array(
                    'option_name' => $option_name,
                    'label_for' => 'ref_indicator_border',
                )
        );
    }
}
