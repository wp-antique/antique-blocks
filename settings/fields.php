<?php
/*
  ----------------------------------------
  Options
  ----------------------------------------
 */

function get_antique_link_boxes_default_general_options() {

    $defaults = array(
        'border_radius' => '4',
        'shadow_bool' => '',
        'ref_bool' => '',
        'ref_numbering' => '(n)',
        'ref_position' => 'ref_page',
    );

    return $defaults;
}

function get_antique_link_boxes_default_single_options($option_name) {

    $default_bg_color = '#4e4e4e';
    $default_color = '#ffffff';
    $default_border_width = '1';
    $default_border_style = 'solid';
    $default_border_color = $default_bg_color;
    $default_padding = '0';
    $default_object_fit = 'cover';
    $default_tag = 'span';
    $default_bool = '';

    $defaults = array(
        'title_tag' => $default_tag,
        'subtitle_tag' => $default_tag,
        'color' => $default_color,
        'bg_color' => $default_bg_color,
        'border_bool' => $default_bool,
        'border_width' => $default_border_width,
        'border_style' => $default_border_style,
        'border_color' => $default_border_color,
        'folded_corner_bool' => $default_bool,
        'img_object_fit' => $default_object_fit,
        'img_padding_tb' => $default_padding,
        'img_padding_lr' => $default_padding,
        'ref_indicator_color' => '#000000',
        'ref_indicator_bg_color' => '#ffffff',
        'ref_indicator_border_bool' => $default_bool,
        'ref_indicator_border_width' => $default_border_width,
        'ref_indicator_border_style' => $default_border_style,
        'ref_indicator_border_color' => '#000000',
    );

    if ($option_name == 'antique_link_box_3_options') {
        unset($defaults['subtitle_tag']);
    }

    return $defaults;
}

function get_antique_link_boxes_option($option_name) {

    if ($option_name == 'antique_link_boxes_general_options') {
        $defaults = get_antique_link_boxes_default_general_options();
    } else {
        $defaults = get_antique_link_boxes_default_single_options(
                option_name: $option_name
        );
    }

    $options = wp_parse_args(
            args: get_option($option_name),
            defaults: $defaults
    );

    return $options;
}

/*
  ----------------------------------------
  General Settings
  ----------------------------------------
 */

function antique_link_boxes_style_section_cb($args) {

}

function antique_link_boxes_field_border_radius_cb($args) {

    $option_name = $args['option_name'];
    $options = get_antique_link_boxes_option(option_name: $option_name);

    $field_id = esc_attr($args['label_for']);
    ?>
    <input type="number"
           id="<?php echo $field_id; ?>"
           name="<?php echo esc_attr($option_name); ?>[<?php echo $field_id; ?>]"
           value="<?php echo esc_attr($options[$field_id]); ?>"
           min="0" max="100"
           style="width: 60px"
           autocomplete="off"
           >
    <label for="<?php echo $field_id; ?>">px</label>

    <p class="description">
        <?php
        esc_html_e('Specify the border radius for all Antique blocks.',
                'antique-link-boxes');
        ?>
    </p>
    <?php
}

function antique_link_boxes_field_shadow_bool_cb($args) {

    $option_name = $args['option_name'];
    $options = get_antique_link_boxes_option(option_name: $option_name);

    $field_id = esc_attr($args['label_for']);
    ?>
    <input type="checkbox"
           id="<?php echo $field_id; ?>"
           name="<?php echo esc_attr($option_name); ?>[<?php echo $field_id; ?>]"
           <?php echo $options[$field_id] != '' ? 'checked="checked"' : ''; ?>
           autocomplete="off"
           >

    <p class="description">
        <?php
        esc_html_e('Add a shadow that appears when hovering over '
                . 'Antique blocks (does not work on smartphones).',
                'antique-link-boxes');
        ?>
    </p>
    <?php
}

function antique_link_boxes_ref_section_cb($args) {
    ?>
    <p>
        <?php
        esc_html_e('If you enable image references, additional options '
                . 'to customize the appearance are displayed on each block '
                . 'settings page.', 'antique-link-boxes');
        ?>
    </p>
    <?php
}

function antique_link_boxes_field_ref_cb($args) {

    $option_name = $args['option_name'];
    $options = get_antique_link_boxes_option(option_name: $option_name);

    $field_id = esc_attr($args['label_for']);
    ?>
    <input type="checkbox"
           class="antique-link-boxes-field-ref"
           id="<?php echo $field_id; ?>"
           name="<?php echo esc_attr($option_name); ?>[<?php echo $field_id; ?>]"
           <?php echo $options[$field_id] != '' ? 'checked="checked"' : ''; ?>
           autocomplete="off"
           >

    <?php
}

function antique_link_boxes_field_ref_position_cb($args) {

    $option_name = $args['option_name'];
    $options = get_antique_link_boxes_option(option_name: $option_name);

    $field_id = esc_attr($args['label_for']);
    ?>

    <select
        id="<?php echo esc_attr($field_id); ?>"
        name="<?php echo esc_attr($option_name); ?>[<?php echo $field_id; ?>]"
        autocomplete="off"
        >
        <option value="ref_page" <?php echo isset($options[$field_id]) ? ( selected($options[$field_id], 'ref_page', false) ) : ( '' ); ?>>
            <?php esc_html_e('end of page', 'antique-link-boxes'); ?>
        </option>
        <option value="ref_block" <?php echo isset($options[$field_id]) ? ( selected($options[$field_id], 'ref_block', false) ) : ( '' ); ?>>
            <?php esc_html_e('below block', 'antique-link-boxes'); ?>
        </option>
    </select>

    <?php
}

function antique_link_boxes_field_ref_numbering_cb($args) {

    $option_name = $args['option_name'];
    $options = get_antique_link_boxes_option(option_name: $option_name);

    $field_id = esc_attr($args['label_for']);
    ?>
    <select
        id="<?php echo $field_id; ?>"
        name="<?php echo esc_attr($option_name); ?>[<?php echo $field_id; ?>]"
        autocomplete="off"
        >
        <option value="[n]" <?php echo isset($options[$field_id]) ? ( selected($options[$field_id], '[n]', false) ) : ( '' ); ?>>
            [n]
        </option>
        <option value="(n)" <?php echo isset($options[$field_id]) ? ( selected($options[$field_id], '(n)', false) ) : ( '' ); ?>>
            (n)
        </option>
        <option value="n" <?php echo isset($options[$field_id]) ? ( selected($options[$field_id], 'n', false) ) : ( '' ); ?>>
            n
        </option>
    </select>
    <?php
}

/*
  ----------------------------------------
  Single Block Settings
  ----------------------------------------
 */

function antique_link_box_section_style_cb($args) {
    ?>

    <p id="<?php echo esc_attr($args['id']); ?>">
        <?php esc_html_e('Customize the block appearance.', 'antique-link-boxes'); ?>
    </p>

    <?php
}

function antique_link_box_field_color_cb($args) {

    $option_name = $args['option_name'];
    $options = get_antique_link_boxes_option(option_name: $option_name);

    $field_id = esc_attr($args['label_for']);
    ?>

    <input type="text"
           class="color-field"
           id="<?php echo $field_id; ?>"
           name="<?php echo esc_attr($option_name); ?>[<?php echo $field_id; ?>]"
           value="<?php echo esc_attr($options[$field_id]); ?>"
           data-default-color="#ffffff"
           autocomplete="off"
           >

    <?php
}

function antique_link_box_field_bg_color_cb($args) {

    $option_name = $args['option_name'];
    $options = get_antique_link_boxes_option(option_name: $option_name);

    $field_id = esc_attr($args['label_for']);
    ?>

    <input type="text"
           class="color-field"
           id="<?php echo $field_id; ?>"
           name="<?php echo esc_attr($option_name); ?>[<?php echo $field_id; ?>]"
           value="<?php echo esc_attr($options[$field_id]); ?>"
           data-default-color="#4e4e4e"
           autocomplete="off"
           >

    <?php
}

function antique_link_box_field_border_cb($args) {

    $option_name = $args['option_name'];
    $options = get_antique_link_boxes_option(option_name: $option_name);

    $field_id = esc_attr($args['label_for']);
    $str_border_bool = esc_attr($field_id . '_bool');
    $str_border_width = esc_attr($field_id . '_width');
    $str_border_style = esc_attr($field_id . '_style');
    $str_border_color = esc_attr($field_id . '_color');
    ?>

    <input type="checkbox"
           class="enable-border"
           id="<?php echo $str_border_bool; ?>"
           name="<?php echo esc_attr($option_name); ?>[<?php echo $str_border_bool; ?>]"
           <?php echo $options[$str_border_bool] != '' ? 'checked="checked"' : ''; ?>
           autocomplete="off"
           >

    <p class="description">
        <?php esc_html_e('Add a border to the block.', 'antique-link-boxes'); ?>
    </p>

    <div class="border-customization">
        <input type="number"
               id="<?php echo $str_border_width; ?>"
               name="<?php echo esc_attr($option_name); ?>[<?php echo $str_border_width; ?>]"
               value="<?php echo esc_attr($options[$str_border_width]); ?>"
               min="1" max="10"
               style="width: 60px"
               autocomplete="off"
               >
        <label for="<?php echo $str_border_width; ?>">
            px
        </label>

        <select id="<?php echo $str_border_style; ?>"
                name="<?php echo esc_attr($option_name); ?>[<?php echo $str_border_style; ?>]"
                autocomplete="off"
                >

            <option value="solid" <?php
            echo isset($options[$str_border_style]) ?
                    ( selected($options[$str_border_style], 'solid', false) ) :
                    ( '' );
            ?>
                    >solid</option>

            <option value="double" <?php
            echo isset($options[$str_border_style]) ?
                    ( selected($options[$str_border_style], 'double', false) ) :
                    ( '' );
            ?>
                    >double</option>
        </select>

        <input type="text"
               class="color-field"
               id="<?php echo $str_border_color; ?>"
               name="<?php echo esc_attr($option_name); ?>[<?php echo $str_border_color; ?>]"
               value="<?php echo esc_attr($options[$str_border_color]); ?>"
               data-default-color="#000000"
               autocomplete="off"
               >
    </div>

    <?php
}

function antique_link_box_field_folded_corner_bool_cb($args) {

    $option_name = $args['option_name'];
    $options = get_antique_link_boxes_option(option_name: $option_name);

    $field_id = esc_attr($args['label_for']);
    ?>

    <input type="checkbox"
           id="<?php echo $field_id; ?>"
           name="<?php echo esc_attr($option_name); ?>[<?php echo $field_id; ?>]"
           <?php echo $options[$field_id] != '' ? 'checked="checked"' : ''; ?>
           autocomplete="off"
           >

    <p class="description">
        <?php esc_html_e('Add a folded corner.', 'antique-link-boxes'); ?>
    </p>

    <?php
}

function antique_link_box_field_img_cb($args) {

    $option_name = $args['option_name'];
    $options = get_antique_link_boxes_option(option_name: $option_name);

    $field_id = esc_attr($args['label_for']);
    $str_object_fit = esc_attr($field_id . '_object_fit');
    $str_padding_tb = esc_attr($field_id . '_padding_tb');
    $str_padding_lr = esc_attr($field_id . '_padding_lr');
    ?>

    <div style="margin-bottom: 10px;">
        <select class="object-fit-setting"
                id="<?php echo $str_object_fit; ?>"
                name="<?php echo esc_attr($option_name); ?>[<?php echo $str_object_fit; ?>]"
                autocomplete="off"
                >

            <option value="contain" <?php
            echo isset($options[$str_object_fit]) ?
                    ( selected($options[$str_object_fit], 'contain', false) ) :
                    ( '' );
            ?>>contain</option>

            <option value="cover" <?php
            echo isset($options[$str_object_fit]) ?
                    ( selected($options[$str_object_fit], 'cover', false) ) :
                    ( '' );
            ?>>cover</option>

        </select>
    </div>

    <div class="object-fit-contain-settings">
        <div style="margin-bottom: 10px;">
            <input id="<?php echo $str_padding_tb; ?>"
                   name="<?php echo esc_attr($option_name); ?>[<?php echo $str_padding_tb; ?>]"
                   type="number" value="<?php echo esc_attr($options[$str_padding_tb]) ?>"
                   min="0" max="100"
                   style="width: 60px"
                   autocomplete="off"
                   >
            <label for="<?php echo $str_padding_tb; ?>">
                px (top, bottom)
            </label>
        </div>
        <div>
            <input id="<?php echo $str_padding_lr; ?>"
                   name="<?php echo esc_attr($option_name); ?>[<?php echo $str_padding_lr; ?>]"
                   type="number" value="<?php echo esc_attr($options[$str_padding_lr]) ?>"
                   min="0" max="100"
                   style="width: 60px"
                   autocomplete="off"
                   >
            <label for="<?php echo $str_padding_lr; ?>">
                px (left, right)
            </label>
        </div>
        <p class="description">
            <?php esc_html_e('Add padding to images.', 'antique-link-boxes'); ?>
        </p>
    </div>

    <?php
}

function antique_link_box_section_structure_cb($args) {
    ?>

    <p id="<?php echo esc_attr($args['id']); ?>">
        <?php esc_html_e('Customize the HTML structure of the block.', 'antique-link-boxes'); ?>
    </p>

    <?php
}

function antique_link_box_field_title_tag_cb($args) {

    $option_name = $args['option_name'];
    $options = get_antique_link_boxes_option(option_name: $option_name);

    $field_id = esc_attr($args['label_for']);
    ?>

    <select
        id="<?php echo $field_id; ?>"
        name="<?php echo esc_attr($option_name); ?>[<?php echo $field_id; ?>]"
        autocomplete="off"
        >

        <option value="span" <?php
        echo isset($options[$field_id]) ?
                ( selected($options[$field_id], 'span', false) ) :
                ( '' );
        ?>>span</option>

        <option value="p" <?php
        echo isset($options[$field_id]) ?
                ( selected($options[$field_id], 'p', false) ) :
                ( '' );
        ?>>p</option>

        <option value="h2" <?php
        echo isset($options[$field_id]) ?
                ( selected($options[$field_id], 'h2', false) ) :
                ( '' );
        ?>>h2</option>

        <option value="h3" <?php
        echo isset($options[$field_id]) ?
                ( selected($options[$field_id], 'h3', false) ) :
                ( '' );
        ?>>h3</option>

    </select>

    <?php
}

function antique_link_box_field_subtitle_tag_cb($args) {

    $option_name = $args['option_name'];
    $options = get_antique_link_boxes_option(option_name: $option_name);

    $field_id = esc_attr($args['label_for']);
    ?>

    <select
        id="<?php echo $field_id; ?>"
        name="<?php echo esc_attr($option_name); ?>[<?php echo $field_id; ?>]"
        autocomplete="off"
        >

        <option value="span" <?php
        echo isset($options[$field_id]) ?
                ( selected($options[$field_id], 'span', false) ) :
                ( '' );
        ?>>span</option>

        <option value="p" <?php
        echo isset($options[$field_id]) ?
                ( selected($options[$field_id], 'p', false) ) :
                ( '' );
        ?>>p</option>

        <option value="h3" <?php
        echo isset($options[$field_id]) ?
                ( selected($options[$field_id], 'h3', false) ) :
                ( '' );
        ?>>h3</option>

        <option value="h4" <?php
        echo isset($options[$field_id]) ?
                ( selected($options[$field_id], 'h4', false) ) :
                ( '' );
        ?>>h4</option>

    </select>

    <?php
}

function antique_link_box_section_ref_cb($args) {

}

function antique_link_box_field_ref_indicator_color_cb($args) {

    $option_name = $args['option_name'];
    $options = get_antique_link_boxes_option(option_name: $option_name);

    $field_id = esc_attr($args['label_for']);
    ?>

    <input type="text"
           class="color-field"
           id="<?php echo $field_id; ?>"
           name="<?php echo esc_attr($option_name); ?>[<?php echo esc_attr($field_id); ?>]"
           value="<?php echo esc_attr($options[$field_id]); ?>"
           data-default-color="#000000"
           autocomplete="off"
           >

    <?php
}

function antique_link_box_field_ref_indicator_bg_color_cb($args) {

    $option_name = $args['option_name'];
    $options = get_antique_link_boxes_option(option_name: $option_name);

    $field_id = esc_attr($args['label_for']);
    ?>

    <input type="text"
           class="color-field"
           id="<?php echo $field_id; ?>"
           name="<?php echo esc_attr($option_name); ?>[<?php echo esc_attr($field_id); ?>]"
           value="<?php echo esc_attr($options[$field_id]); ?>"
           data-default-color="#ffffff"
           autocomplete="off"
           >

    <?php
}

function antique_link_box_field_ref_indicator_border_cb($args) {

    $option_name = $args['option_name'];
    $options = get_antique_link_boxes_option($option_name);

    $field_id = esc_attr($args['label_for']);
    $str_border_bool = esc_attr($field_id . '_bool');
    $str_border_width = esc_attr($field_id . '_width');
    $str_border_style = esc_attr($field_id . '_style');
    $str_border_color = esc_attr($field_id . '_color');
    ?>

    <input type="checkbox"
           class="enable-border"
           id="<?php echo $str_border_bool; ?>"
           name="<?php echo esc_attr($option_name); ?>[<?php echo $str_border_bool; ?>]"
           <?php echo $options[$str_border_bool] != '' ? 'checked="checked"' : ''; ?>
           autocomplete="off"
           >

    <div class="border-customization">
        <input type="number"
               id="<?php echo $str_border_width; ?>"
               name="<?php echo esc_attr($option_name); ?>[<?php echo $str_border_width; ?>]"
               value="<?php echo esc_attr($options[$str_border_width]); ?>"
               min="1" max="10"
               style="width: 60px"
               autocomplete="off"
               >
        <label for="<?php echo $str_border_width; ?>">
            px
        </label>

        <select id="<?php echo $str_border_style; ?>"
                name="<?php echo esc_attr($option_name); ?>[<?php echo $str_border_style; ?>]"
                autocomplete="off"
                >

            <option
                value="solid"
                <?php
                echo isset($options[$str_border_style]) ?
                        ( selected($options[$str_border_style], 'solid', false) ) :
                        ( '' );
                ?>
                >solid</option>

            <option
                value="double"
                <?php
                echo isset($options[$str_border_style]) ?
                        ( selected($options[$str_border_style], 'double', false) ) :
                        ( '' );
                ?>
                >double</option>

        </select>

        <input type="text"
               class="color-field"
               id="<?php echo $str_border_color; ?>"
               name="<?php echo esc_attr($option_name); ?>[<?php echo $str_border_color; ?>]"
               value="<?php echo esc_attr($options[$str_border_color]); ?>"
               data-default-color="#000000"
               autocomplete="off"
               >
    </div>

    <?php
}
