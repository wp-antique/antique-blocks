(function () {
    var object_fit = document.querySelector(".object-fit-setting");
    var contain_settings = document.querySelector(".object-fit-contain-settings");

    function monitor_object_fit(option) {

        if (option === "contain") {
            contain_settings.style.display = "block";
        } else {
            contain_settings.style.display = "none";
        }

    }

    if (object_fit) {
        monitor_object_fit(object_fit.value);

        object_fit.addEventListener("change", function () {
            monitor_object_fit(object_fit.value);
        });
    }

    var display_refs = document.querySelector(".antique-link-boxes-field-ref");
    var ref_settings = document.querySelectorAll(".antique-link-boxes-ref-settings");

    function monitor_ref_display(is_enabled) {

        if (is_enabled) {
            ref_settings.forEach(function (ref_setting) {
                ref_setting.style.display = "table-row";
            });
        } else {

            ref_settings.forEach(function (ref_setting) {
                ref_setting.style.display = "none";
            });
        }

    }

    if (display_refs) {

        monitor_ref_display(display_refs.checked);

        display_refs.addEventListener("change", function () {
            monitor_ref_display(display_refs.checked);
        });
    }
})($);