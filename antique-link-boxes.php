<?php

/**
 * Plugin Name: Antique Link Boxes
 * Plugin URI: https://gitlab.com/wp-antique/plugins/antique-link-boxes
 * Description: ...
 * Version: 1.0.0
 * Author: Simon Garbin
 * Author URI: https://simongarbin.com
 * License: GPLv3
 * License URI: https://www.gnu.org/licenses/gpl-3.0.html.en
 * Text Domain: antique-link-boxes
 * Domain Path: /languages
 */
/*
  Copyright 2023 Simon Garbin
  Antique Link Boxes is free software: you can redistribute it and/or
  modify it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  any later version.

  Antique Link Boxes is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Antique Link Boxes. If not, see
  https://www.gnu.org/licenses/gpl-3.0.html.en.
 */
/*
  Comments:
  - Every function is prefixed by the plugin name in order to prevent
  name collisions.
  - In almost all functions named arguments are used in order to facilitate
  the understanding of the code.
  - Single quotation marks are used for php/js code, double quotation marks
  for HTML.
 */

include plugin_dir_path(__FILE__) . '/shared/menu.php';
include plugin_dir_path(__FILE__) . '/shared/register.php';
include plugin_dir_path(__FILE__) . '/settings/settings.php';

defined('ABSPATH') || exit;

/*
  ----------------------------------------
  Translation
  ----------------------------------------
 */

add_action(
        hook_name: 'init',
        callback: 'antique_link_boxes_load_textdomain'
);

function antique_link_boxes_load_textdomain() {
    load_plugin_textdomain(
            domain: 'antique-link-boxes',
            deprecated: false,
            plugin_rel_path: dirname(plugin_basename(__FILE__)) . '/languages'
    );
}

/*
  ----------------------------------------
  Admin Settings
  ----------------------------------------
 */

add_filter(
        hook_name: 'plugin_action_links_' . plugin_basename(__FILE__),
        callback: 'antique_link_boxes_settings_link'
);

function antique_link_boxes_settings_link(array $links) {

    $url = admin_url(path: 'admin.php?page=antique_link_boxes&tab=general_options');
    $link = '<a href="' . esc_html($url) . '">'
            . __('Settings', 'antique-link-boxes')
            . '</a>';
    $links[] = $link;

    return $links;
}

/*
  ----------------------------------------
  Link Boxes
  ----------------------------------------
 */

add_action(
        hook_name: 'init',
        callback: 'antique_link_boxes_register_blocks'
);

function antique_link_boxes_register_blocks() {

    register_block_type(
            block_type: plugin_dir_path(__FILE__) . '/blocks/block-1/'
    );
    register_block_type(
            block_type: plugin_dir_path(__FILE__) . '/blocks/block-2/'
    );
    register_block_type(
            block_type: plugin_dir_path(__FILE__) . '/blocks/block-3/'
    );
}

/*
  ----------------------------------------
  Styles and Scripts
  ----------------------------------------
 */

add_action(
        hook_name: 'wp_head',
        callback: 'antique_link_boxes_custom_style'
);

add_action(
        hook_name: 'enqueue_block_editor_assets',
        callback: 'antique_link_boxes_custom_style'
);

function antique_link_boxes_custom_style() {

    $style = '<style id="antique-link-boxes-custom-style">'
            . ":root {\n";

    $theme = wp_get_theme();

    if ($theme == 'Antique') {
        $style .= "\t--antique-link-boxes-heading-font: var(--antique-secondary-font);\n";
    } else {
        $style .= "\t--antique-link-boxes-heading-font: sans-serif;\n";
    }

    $options_general = get_antique_link_boxes_option(
            option_name: 'antique_link_boxes_general_options'
    );
    $style .= "\t--antique-link-boxes-border-radius: "
            . $options_general['border_radius'] . "px;\n";

    for ($i = 1; $i <= 3; $i++) {

        $nr = strval($i);
        $options_single = get_antique_link_boxes_option(
                option_name: 'antique_link_box_' . $nr . '_options'
        );

        foreach ($options_single as $key => $value) {

            if (str_contains($key, 'bool')) {
                continue;
            }

            $css_var = "\t--antique-link-box-" . $nr;
            $css_var .= "-" . str_replace("_", "-", $key);
            $style .= $css_var . ": " . $value;

            if (
                    str_contains($key, 'width') or
                    str_contains($key, 'radius') or
                    str_contains($key, 'padding')
            ) {
                $style .= "px;\n";
            } else {
                $style .= ";\n";
            }
        }
    }

    $style .= "}"
            . "</style>";

    echo $style;
}

add_action(
        hook_name: 'init',
        callback: 'antique_link_boxes_shared_style'
);

function antique_link_boxes_shared_style() {

    $handle = 'antique-link-boxes-shared-style';

    wp_register_style(
            handle: $handle,
            src: plugins_url('/assets/css/shared-style.css', __FILE__),
            deps: array(),
            ver: false,
            media: 'all'
    );
}

add_action(
        hook_name: 'init',
        callback: 'antique_link_boxes_shared_editor'
);

function antique_link_boxes_shared_editor() {

    $handle = 'antique-link-boxes-shared-editor';

    wp_register_style(
            handle: $handle,
            src: plugins_url('/assets/css/shared-editor.css', __FILE__),
            deps: array(),
            ver: false,
            media: 'all'
    );
}

add_action(
        hook_name: 'init',
        callback: 'antique_link_box_1_apply_options'
);

function antique_link_box_1_apply_options() {

    $options = get_antique_link_boxes_option(
            option_name: 'antique_link_box_1_options'
    );

    $object_fit = $options['img_object_fit'];
    $has_padding = ($object_fit === 'contain') ? true : false;

    $antique_link_box_1_options = array(
        'title_tag' => $options['title_tag'],
        'subtitle_tag' => $options['subtitle_tag'],
        'block_has_border' => $options['border_bool'],
        'block_has_folded_corner' => $options['folded_corner_bool'],
        'img_has_padding' => $has_padding,
        'ref_has_border' => $options['ref_indicator_border_bool'],
    );

    $handle = 'antique-link-box-1-apply-options';

    wp_register_script(
            handle: $handle,
            src: plugins_url('/assets/js/block-1-apply.js', __FILE__),
            deps: array(),
            ver: false,
            in_footer: true
    );

    wp_localize_script(
            handle: $handle,
            object_name: 'antique_link_box_1_options',
            l10n: $antique_link_box_1_options
    );
}

add_action(
        hook_name: 'init',
        callback: 'antique_link_box_2_apply_options'
);

function antique_link_box_2_apply_options() {

    $options = get_antique_link_boxes_option(
            option_name: 'antique_link_box_2_options'
    );

    $object_fit = $options['img_object_fit'];
    $has_padding = ($object_fit === 'contain') ? true : false;

    $antique_link_box_2_options = array(
        'title_tag' => $options['title_tag'],
        'subtitle_tag' => $options['subtitle_tag'],
        'block_has_border' => $options['border_bool'],
        'block_has_folded_corner' => $options['folded_corner_bool'],
        'img_has_padding' => $has_padding,
        'ref_has_border' => $options['ref_indicator_border_bool'],
    );

    $handle = 'antique-link-box-2-apply-options';

    wp_register_script(
            handle: $handle,
            src: plugins_url('/assets/js/block-2-apply.js', __FILE__),
            deps: array(),
            ver: false,
            in_footer: true
    );

    wp_localize_script(
            handle: $handle,
            object_name: 'antique_link_box_2_options',
            l10n: $antique_link_box_2_options
    );
}

add_action(
        hook_name: 'init',
        callback: 'antique_link_box_3_apply_options'
);

function antique_link_box_3_apply_options() {

    $options = get_antique_link_boxes_option(
            option_name: 'antique_link_box_3_options'
    );

    $object_fit = $options['img_object_fit'];
    $has_padding = ($object_fit === 'contain') ? true : false;

    $antique_link_box_3_options = array(
        'title_tag' => $options['title_tag'],
        'block_has_border' => $options['border_bool'],
        'block_has_folded_corner' => $options['folded_corner_bool'],
        'img_has_padding' => $has_padding,
        'ref_has_border' => $options['ref_indicator_border_bool'],
    );

    $handle = 'antique-link-box-3-apply-options';

    wp_register_script(
            handle: $handle,
            src: plugins_url('/assets/js/block-3-apply.js', __FILE__),
            deps: array(),
            ver: false,
            in_footer: true
    );

    wp_localize_script(
            handle: $handle,
            object_name: 'antique_link_box_3_options',
            l10n: $antique_link_box_3_options
    );
}

add_action(
        hook_name: 'init',
        callback: 'antique_link_boxes_apply_shadow_options'
);

function antique_link_boxes_apply_shadow_options() {

    $is_shadow_on = get_antique_link_boxes_option(
                    option_name: 'antique_link_boxes_general_options'
            )['shadow_bool'];

    $antique_link_boxes_shadow_options = array(
        'is_shadow_on' => $is_shadow_on
    );

    $handle = 'antique-link-boxes-apply-shadow';

    wp_register_script(
            handle: $handle,
            src: plugins_url('/assets/js/shadow.js', __FILE__),
            deps: array(),
            ver: false,
            in_footer: true
    );

    wp_localize_script(
            handle: $handle,
            object_name: 'antique_link_boxes_shadow_options',
            l10n: $antique_link_boxes_shadow_options
    );
}

add_action(
        hook_name: 'init',
        callback: 'antique_link_boxes_apply_ref_options'
);

function antique_link_boxes_apply_ref_options() {

    $options = get_antique_link_boxes_option(
            option_name: 'antique_link_boxes_general_options'
    );

    $antique_link_boxes_ref_options = array(
        'enabled' => $options['ref_bool'],
        'position' => $options['ref_position'],
        'numbering' => $options['ref_numbering'],
    );

    $handle = 'antique-link-boxes-apply-refs';

    wp_register_script(
            handle: $handle,
            src: plugins_url('/assets/js/references.js', __FILE__),
            deps: array(),
            ver: false,
            in_footer: true
    );

    wp_localize_script(
            handle: $handle,
            object_name: 'antique_link_boxes_ref_options',
            l10n: $antique_link_boxes_ref_options
    );
}
