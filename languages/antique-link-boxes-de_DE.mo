��    6      �  I   |      �     �     �     �  [   �     1     H     [     n     �     �     �     �     �     �  *   �          %     -     <     H     M  y   P     �  
   �  
   �  -   �  	     
      
   +     6     C     I     V     _     g  1   l  	   �     �     �     �     �  
   �     �     �                (     4     9     B     G     L     R  X  W     �     �     �  |   �  )   e     �     �     �     �     �     �  
   �            &     )   D     n  	   z     �  
   �     �  �   �     M  
   U     `  2   n  	   �     �     �     �     �     �     �  	   
       -        K     R  
   X     c     s  
   y  *   �  '   �     �     �  
   �     �     �                               6   1   -      
      3                            !   "                             	   %             0   '   #   (   2          5              .   &            )   +      *                /   $              4            ,                                ... Add a border to the block. Add a folded corner. Add a shadow that appears when hovering over Antique blocks (does not work on smartphones). Add padding to images. Antique Link Box 1 Antique Link Box 2 Antique Link Box 3 Antique Link Boxes Antique Plugins Background color: Border radius: Border: Choose image Customize the HTML structure of the block. Customize the block appearance. Enable: Folded corner: Font color: Hide ID If you enable image references, additional options to customize the appearance are displayed on each block settings page. Images: Link Boxes Numbering: Pages and posts containing one of the blocks: Position: Reference: References Remove image Reset Save changes Settings Shadow: Show Specify the border radius for all Antique blocks. Structure Style Subtitle Subtitle tag: Title Title tag: Your settings have been reset. Your settings have been saved. below block content end of page link location page post title type Project-Id-Version: Antique Link Boxes
PO-Revision-Date: 2023-05-14 23:26+0200
Last-Translator: 
Language-Team: Simon Garbin
Language: de_DE
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Poedit 3.0.1
X-Poedit-Basepath: ..
X-Poedit-KeywordsList: __;_e;esc_html_e;esc_html__;esc_attr__;esc_attr_e
X-Poedit-SearchPath-0: antique-link-boxes.php
X-Poedit-SearchPath-1: settings/fields.php
X-Poedit-SearchPath-2: settings/settings.php
X-Poedit-SearchPath-3: blocks/block-1/block.js
X-Poedit-SearchPath-4: blocks/block-1/block.json
X-Poedit-SearchPath-5: blocks/block-2/block.js
X-Poedit-SearchPath-6: blocks/block-2/block.json
X-Poedit-SearchPath-7: blocks/block-3/block.js
X-Poedit-SearchPath-8: blocks/block-3/block.json
X-Poedit-SearchPath-9: shared/menu.php
 ... Füge einen Rahmen hinzu. Füge ein Eselsohr hinzu. Füge einen Schatten hinzu, der erscheint, wenn sich der Cursor auf dem Block befindet (funktioniert auf Smartphones nicht). Füge einen Innenabstand (padding) hinzu. Antique Link Box 1 Antique Link Box 2 Antique Link Box 3 Antique Link-Boxen Antique Plugins Hintergrundfarbe: Eckradius: Rahmen: Bild wählen Passe die HTML-Struktur des Blocks an. Passe das Erscheinungsbild des Blocks an. Aktivieren: Eselsohr: Schriftfarbe: Verstecken ID Wenn du Quellenangaben für Bilder aktivierst, erscheinen für jeden Block zusätzliche Einstellungen, mit denen du das Erscheinungsbild der Quellenangaben anpassen kannst. Bilder: Link-Boxen Nummerierung: Seiten und Posts, die einen der Blöcke enthalten: Position: Quellenangabe: Quellenangaben Bild entfernen Zurücksetzen Änderungen speichern Einstellungen Schatten: Anzeigen Lege den Eckradius für alle Link-Boxen fest. Aufbau Style Untertitel Untertitel-Tag: Titel Titel-Tag: Deine Einstellungen wurden zurückgesetzt. Deine Einstellungen wurden gespeichert. unter Block Inhalt Seitenende Link Stelle Seite Post Titel Typ 